#include "stdafx.h"
#include "SaveLoadData.h"

bool InitIniFile()
{
	WCHAR filePath[MAX_FILE_PATH];
	GetCurrentDirectory(MAX_FILE_PATH, filePath);
	StrCat(filePath, L"\\data.ini");

	DWORD byteWrittenCount;
	if (!PathFileExists(filePath))
	{
		HANDLE fileHandle = CreateFile(filePath, GENERIC_READ | GENERIC_WRITE,
			0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
		WriteFile(fileHandle, &BOM, sizeof(BOM), &byteWrittenCount, NULL);
		CloseHandle(fileHandle);

		WCHAR* currentDate = GetCurrentDate();
		WritePrivateProfileString(L"globalvar", L"date", currentDate, filePath);
		if (currentDate != NULL)
			delete currentDate;

		return true;
	}
	else
		return false;
}

void ResetIniFile(HWND dateLabel)
{
	WCHAR filePath[MAX_FILE_PATH];
	GetCurrentDirectory(MAX_FILE_PATH, filePath);
	StrCat(filePath, L"\\data.ini");

	DWORD byteWrittenCount;
	HANDLE fileHandle = CreateFile(filePath, GENERIC_READ | GENERIC_WRITE,
		0, NULL, TRUNCATE_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	WriteFile(fileHandle, &BOM, sizeof(BOM), &byteWrittenCount, NULL);
	CloseHandle(fileHandle);

	WCHAR* currentDate = new WCHAR[15];
	ZeroMemory(currentDate, 15 * sizeof(WCHAR));
	GetWindowText(dateLabel, currentDate, 15);
	WritePrivateProfileString(L"globalvar", L"date", currentDate, filePath);
	if (currentDate != NULL)
		delete currentDate;
}

void GetDataFromFile(HWND listviewHandle, int &lastItemIndex, int &currentItemIndex,
	int &totalexpense, vector<int> &expensePerType, vector<int> &percentages,
	bool &changeFlag)
{
	WCHAR filePath[MAX_FILE_PATH];
	GetCurrentDirectory(MAX_FILE_PATH, filePath);
	StrCat(filePath, L"\\data.ini");

	WCHAR buffer[MAX_LOADSTRING];
	ZeroMemory(buffer, MAX_LOADSTRING * sizeof(WCHAR));

	lastItemIndex = 0;

	// Get count of item
	GetPrivateProfileString(L"globalvar", L"count", L"", 
		buffer, MAX_LOADSTRING, filePath);
	if (wcslen(buffer) != 0)
		currentItemIndex = _wtoi(buffer);
	else
		currentItemIndex = 0;

	while (lastItemIndex < currentItemIndex)
	{
		WCHAR typeStr[5];
		ZeroMemory(typeStr, 5 * sizeof(WCHAR));
		WCHAR contentStr[MAX_LOADSTRING];
		ZeroMemory(contentStr, MAX_LOADSTRING * sizeof(WCHAR));
		WCHAR costStr[MAX_LOADSTRING];
		ZeroMemory(costStr, MAX_LOADSTRING * sizeof(WCHAR));

		WCHAR section[MAX_LOADSTRING];
		wsprintf(section, L"item%d", lastItemIndex);
		GetPrivateProfileString(section, L"type", L"",
			typeStr, 5, filePath);
		GetPrivateProfileString(section, L"content", L"",
			contentStr, MAX_LOADSTRING, filePath);
		GetPrivateProfileString(section, L"cost", L"",
			costStr, MAX_LOADSTRING, filePath);

		if (wcslen(typeStr) == 0 || wcslen(contentStr) == 0 || wcslen(costStr) == 0)
			return;
		else
		{
			int typeValue = _wtoi(typeStr);
			AddAnItem(totalexpense, expensePerType, percentages, changeFlag,
				listviewHandle, lastItemIndex, typeValue, contentStr, costStr);
		}
	}
}

void SaveDataToFile(HWND listviewHandle, int &lastItemIndex, int &currentItemIndex)
{
	WCHAR filePath[MAX_FILE_PATH];
	GetCurrentDirectory(MAX_FILE_PATH, filePath);
	StrCat(filePath, L"\\data.ini");

	WCHAR section[MAX_LOADSTRING];
	WCHAR temp[MAX_LOADSTRING];

	ZeroMemory(temp, MAX_LOADSTRING * sizeof(WCHAR));
	wsprintf(temp, L"%d", currentItemIndex);
	WritePrivateProfileString(L"globalvar", L"count", temp, filePath);

	while (lastItemIndex < currentItemIndex)
	{
		ZeroMemory(section, MAX_LOADSTRING * sizeof(WCHAR));
		wsprintf(section, L"item%d", lastItemIndex);
		LVITEM lvItem;
		lvItem.mask = LVIF_PARAM;

		
		lvItem.iItem = lastItemIndex;
		lvItem.iSubItem = 0;
		ListView_GetItem(listviewHandle, &lvItem);
		ZeroMemory(temp, MAX_LOADSTRING * sizeof(WCHAR));
		wsprintf(temp, L"%d", (int)lvItem.lParam);
		WritePrivateProfileString(section, L"type", temp, filePath);

		ZeroMemory(temp, MAX_LOADSTRING * sizeof(WCHAR));
		ListView_GetItemText(listviewHandle, lastItemIndex, 1, temp, MAX_LOADSTRING);
		WritePrivateProfileString(section, L"content", temp, filePath);
		
		ZeroMemory(temp, MAX_LOADSTRING * sizeof(WCHAR));
		ListView_GetItemText(listviewHandle, lastItemIndex, 2, temp, MAX_LOADSTRING);
		WritePrivateProfileString(section, L"cost", temp, filePath);

		lastItemIndex++;
	}
}

WCHAR* GetCurrentDate()
{
	time_t current_time = time(NULL);
	tm time_ptr;

	localtime_s(&time_ptr, &current_time);

	WCHAR* buffer = new WCHAR[15];
	ZeroMemory(buffer, 15 * sizeof(WCHAR));
	wsprintf(buffer, L"%d/%d/%d", time_ptr.tm_mday, time_ptr.tm_mon + 1, time_ptr.tm_year + 1900);

	return buffer;
}

WCHAR* GetStartDateFromFile()
{
	WCHAR filePath[MAX_FILE_PATH];
	GetCurrentDirectory(MAX_FILE_PATH, filePath);
	StrCat(filePath, L"\\data.ini");

	WCHAR* buffer = new WCHAR[15];
	ZeroMemory(buffer, 15 * sizeof(WCHAR));

	GetPrivateProfileString(L"globalvar", L"date", L"",
		buffer, 15, filePath);

	return buffer;
}