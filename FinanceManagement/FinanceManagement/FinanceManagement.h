#pragma once

#include "resource.h"



// Window message mapping
BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hwnd);
void OnClose(HWND hwnd);


//
//	FUNCTION:	void DrawChart (_In_ HDC&, _In_ vector<COLORREF>&)
//
//	PURPOSE:	Draw statistics chart for expenses.
//
void DrawChart(HDC hdc, vector<COLORREF> &colors);

