// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <CommCtrl.h>
#include <windowsx.h>
#include <WinUser.h>
#include <WinBase.h>
#include <CommCtrl.h>
#include <vector>
#include <fstream>
#include <Shlwapi.h>
#include <ObjIdl.h>
#include <gdiplus.h>
#include <ctime>
#include "SaveLoadData.h"
#include "ListView.h"
using namespace std;
using namespace Gdiplus;

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "Comctl32.lib")
#pragma comment(lib, "Shlwapi.lib")
#pragma comment(lib, "Kernel32.lib")
#pragma comment (lib,"Gdiplus.lib")

// TODO: reference additional headers your program requires here
#define MAX_LOADSTRING 100
#define DEGREE_PER_PERCENT 3.6
#define MAX_FILE_PATH 1024

const unsigned short BOM = 0xFEFF;

enum TypeOfExpense
{
	food,
	transport,
	living,
	vehicle,
	necessary,
	service
};

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);