#pragma once
#include <vector>
using namespace std;

//
//	FUNCTION: void ComputePercentage(_In_ int, _In_ int, _In_Out_ int&,
//									_Out_ vector<int>&, _Out_ vector<int>&)
//
//	PURPOSE: Compute percentage for each type of expense.
//
//	PARAMETER:
//
//			int				type:				the type of expense
//			int				cost:				the value of expense
//			int&			totalExpense:		total of expense
//			vector<int>&	expensePerType:		value of each type
//			vector<int>&	percentages:		percent of each type
//
void ComputePercentage(int type, int cost, int &totalexpense,
	vector<int> &expensePerType, vector<int> &percentages);

//
//	FUNCTION: void AddAnItem(_In_Out_ int&, _Out_ vector<int>&, _Out_ vector<int>&
//							_Out_ bool&, _Out_ HWND, _In_Out_ int&, 
//							_In_ int&, _In_ WCHAR*, _In_ WCHAR*)
//
//	PURPOSE: Add an item to list view using data from file.
//
//	PARAMETER:
//
//			int&			totalExpense:		total of expense
//			vector<int>&	expensePerType:		value of each type
//			vector<int>&	percentages:		percent of each type
//			bool&			changeFlag:			the flag of chance of data
//			HWND			listviewHandle:		the handle of listview to update
//			int&			currentIndex:		the index of item in list view for adding
//			int&			type:				the type of expense
//			WCHAR*			content:			the content of expense
//			WCHAR*			cost:				the cost of expense
//
void AddAnItem(int &totalexpense, vector<int> &expensePerType, vector<int> &percentages,
	bool &changeFlag, HWND listviewHandle, int &currentIndex, int &type, WCHAR* content, WCHAR* cost);

//
//	FUNCTION: void AddAnItem(_In_Out_ int&, _Out_ vector<int>&, _Out_ vector<int>&
//							_Out_ bool&, _Out_ HWND, _In_Out_ int&, 
//							_In_ HWND, _In_ HWND, _In_ HWND)
//
//	PURPOSE: Add an item to list view using data from control.
//
//	PARAMETER:
//
//			int&			totalExpense:		total of expense
//			vector<int>&	expensePerType:		value of each type
//			vector<int>&	percentages:		percent of each type
//			bool&			changeFlag:			the flag of chance of data
//			HWND			listviewHandle:		the handle of listview to update
//			int&			currentIndex:		the index of item in list view for adding
//			HWND			typeCombobox:		the type of expense
//			HWND			contentTextbox:		the content of expense
//			HWND			costTextbox:		the cost of expense
//
void AddAnItem(int &totalexpense, vector<int> &expensePerType, vector<int> &percentages,
	bool &changeFlag, HWND listviewHandle, int &currentIndex,
	HWND typeCombobox, HWND contentTextbox, HWND costTextbox);