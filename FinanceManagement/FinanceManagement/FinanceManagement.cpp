﻿// FinanceManagement.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "FinanceManagement.h"

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND g_hWnd;

// Handle of control
HWND g_typeCombobox;
HWND g_contentTextbox;
HWND g_costTextbox;
HWND g_expenseListview;
HWND g_totalExpenseTextbox;
HWND g_addExpenseButton;
HWND g_clearButton;
HWND g_exitButton;
vector<HWND> g_percentageLabels;
HWND g_startDateLabel;

// Expense variable
int g_totalExpense = 0;
vector<int> g_expensePerType(6, 0);
vector<int> g_percentages(6, 0);

// Rectangle to draw chart
RECT g_chartRect;

// The index of last and latest item in list view
int g_lastIndex = 0;
int g_currentIndex = 0;

// Flag to check if change in content or not
bool g_chanceFlag = false;

// Flag to check if clear content or not
bool g_clearFlag = false;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.
	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR           gdiplusToken;

	// Initialize GDI+.
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_FINANCEMANAGEMENT, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_FINANCEMANAGEMENT));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        //if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		if (!IsDialogMessage(g_hWnd, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

	GdiplusShutdown(gdiplusToken);
    return (int) msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= 0; //CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_FINANCEMANAGEMENT));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_FINANCEMANAGEMENT);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   g_hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      30, 10, 650, 730, nullptr, nullptr, hInstance, nullptr);

   if (!g_hWnd)
   {
      return FALSE;
   }

   ShowWindow(g_hWnd, nCmdShow);
   UpdateWindow(g_hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_CLOSE, OnClose);
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	INITCOMMONCONTROLSEX icex;

	// Ensure that the common control DLL is loaded. 
	icex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC = ICC_LISTVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	// Get system font
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = CreateFont(lf.lfHeight, lf.lfWidth,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);

	// Add combo box
	g_typeCombobox = CreateWindow(L"combobox", L"",
		WS_CHILD | WS_VISIBLE | WS_BORDER | CBS_DROPDOWNLIST | WS_TABSTOP,
		30, 60, 120, 100,
		hWnd, (HMENU)ID_CBBTYPE, hInst, NULL);
	SendMessage(g_typeCombobox, WM_SETFONT, (WPARAM)hFont, NULL);
	ComboBox_AddString(g_typeCombobox, L"Ăn uống");
	ComboBox_AddString(g_typeCombobox, L"Di chuyển");
	ComboBox_AddString(g_typeCombobox, L"Nhà cửa");
	ComboBox_AddString(g_typeCombobox, L"Xe cộ");
	ComboBox_AddString(g_typeCombobox, L"Nhu yếu phẩm");
	ComboBox_AddString(g_typeCombobox, L"Dịch vụ");
	ComboBox_SetCurSel(g_typeCombobox, 0);
	SetFocus(g_typeCombobox);
	// End of add combo box

	// Add textboxs
	g_contentTextbox = CreateWindow(L"edit", L"",
		WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP,
		190, 60, 180, 20,
		hWnd, NULL, hInst, NULL);
	SendMessage(g_contentTextbox, WM_SETFONT, (WPARAM)hFont, NULL);

	g_costTextbox = CreateWindow(L"edit", NULL,
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_NUMBER | WS_TABSTOP,
		400, 60, 100, 20,
		hWnd, NULL, hInst, NULL);
	SendMessage(g_costTextbox, WM_SETFONT, (WPARAM)hFont, NULL);

	g_totalExpenseTextbox = CreateWindow(L"edit", NULL,
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_CENTER | ES_READONLY,
		270, 410, 100, 20,
		hWnd, NULL, hInst, NULL);
	SendMessage(g_totalExpenseTextbox, WM_SETFONT, (WPARAM)hFont, NULL);
	// End of add text box

	// Add buttons
	g_addExpenseButton = CreateWindow(L"button", L"Thêm",
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_TABSTOP,
		530, 60, 80, 20,
		hWnd, (HMENU)IDB_ADD, hInst, NULL);
	SendMessage(g_addExpenseButton, WM_SETFONT, (WPARAM)hFont, NULL);

	g_clearButton = CreateWindow(L"button", L"Xóa",
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_TABSTOP,
		230, 640, 80, 20,
		hWnd, (HMENU)IDB_CLEAR, hInst, NULL);
	SendMessage(g_clearButton, WM_SETFONT, (WPARAM)hFont, NULL);

	g_exitButton = CreateWindow(L"button", L"Thoát",
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_TABSTOP,
		340, 640, 80, 20,
		hWnd, (HMENU)IDB_EXIT, hInst, NULL);
	SendMessage(g_exitButton, WM_SETFONT, (WPARAM)hFont, NULL);
	// End of add buttons

	// Add list view
	g_expenseListview = CreateWindow(WC_LISTVIEWW, L"",
		WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_BORDER | LVS_REPORT,
		30, 150, 580, 180,
		hWnd, NULL, hInst, NULL);
	SendMessage(g_expenseListview, WM_SETFONT, (WPARAM)hFont, NULL);

	LVCOLUMN lvcol;
	lvcol.mask = LVCF_TEXT | LVCF_FMT | LVCF_WIDTH;
	lvcol.pszText = L"Loại chi tiêu";
	lvcol.fmt = LVCFMT_LEFT;
	lvcol.cx = 150;
	ListView_InsertColumn(g_expenseListview, 0, &lvcol);

	lvcol.pszText = L"Nội dung";
	lvcol.cx = 300;
	ListView_InsertColumn(g_expenseListview, 1, &lvcol);

	lvcol.pszText = L"Số tiền";
	lvcol.cx = 130;
	ListView_InsertColumn(g_expenseListview, 2, &lvcol);
	// End of add list view

	// Add label
	HWND labelTemp;

	labelTemp = CreateWindow(L"static", L"Thêm một chi tiêu",
		WS_CHILD | WS_VISIBLE,
		10, 10, 100, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);

	labelTemp = CreateWindow(L"static", L"Loại chi tiêu",
		WS_CHILD | WS_VISIBLE,
		30, 35, 100, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);

	labelTemp = CreateWindow(L"static", L"Nội dung chi tiêu",
		WS_CHILD | WS_VISIBLE,
		190, 35, 100, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);

	labelTemp = CreateWindow(L"static", L"Số tiền",
		WS_CHILD | WS_VISIBLE,
		400, 35, 100, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);

	labelTemp = CreateWindow(L"static", L"Danh sách các chi tiêu từ ngày",
		WS_CHILD | WS_VISIBLE,
		10, 120, 150, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);

	g_startDateLabel = CreateWindow(L"static", L"",
		WS_CHILD | WS_VISIBLE,
		165, 120, 100, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(g_startDateLabel, WM_SETFONT, (WPARAM)hFont, NULL);

	labelTemp = CreateWindow(L"static", L"Thống kê chi tiêu",
		WS_CHILD | WS_VISIBLE,
		10, 380, 100, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);

	labelTemp = CreateWindow(L"static", L"Tổng chi tiêu",
		WS_CHILD | WS_VISIBLE,
		290, 390, 100, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);

	labelTemp = CreateWindow(L"static", L"Ăn uống:",
		WS_CHILD | WS_VISIBLE,
		310, 470, 100, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);

	labelTemp = CreateWindow(L"static", L"0 %%",
		WS_CHILD | WS_VISIBLE,
		390, 470, 30, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);
	g_percentageLabels.push_back(labelTemp);

	labelTemp = CreateWindow(L"static", L"Di chuyển:",
		WS_CHILD | WS_VISIBLE,
		310, 520, 100, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);

	labelTemp = CreateWindow(L"static", L"0 %%",
		WS_CHILD | WS_VISIBLE,
		390, 520, 30, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);
	g_percentageLabels.push_back(labelTemp);

	labelTemp = CreateWindow(L"static", L"Nhà cửa:",
		WS_CHILD | WS_VISIBLE,
		310, 570, 100, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);

	labelTemp = CreateWindow(L"static", L"0 %%",
		WS_CHILD | WS_VISIBLE,
		390, 570, 30, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);
	g_percentageLabels.push_back(labelTemp);

	labelTemp = CreateWindow(L"static", L"Xe cộ:",
		WS_CHILD | WS_VISIBLE,
		480, 470, 100, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);

	labelTemp = CreateWindow(L"static", L"0 %%",
		WS_CHILD | WS_VISIBLE,
		560, 470, 30, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);
	g_percentageLabels.push_back(labelTemp);

	labelTemp = CreateWindow(L"static", L"Nhu yếu phẩm:",
		WS_CHILD | WS_VISIBLE,
		480, 520, 100, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);

	labelTemp = CreateWindow(L"static", L"0 %%",
		WS_CHILD | WS_VISIBLE,
		560, 520, 30, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);
	g_percentageLabels.push_back(labelTemp);

	labelTemp = CreateWindow(L"static", L"Dịch vụ:",
		WS_CHILD | WS_VISIBLE,
		480, 570, 100, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);

	labelTemp = CreateWindow(L"static", L"0 %%",
		WS_CHILD | WS_VISIBLE,
		560, 570, 30, 13,
		hWnd, NULL, hInst, NULL);
	SendMessage(labelTemp, WM_SETFONT, (WPARAM)hFont, NULL);
	g_percentageLabels.push_back(labelTemp);
	// End of add labels

	// Create chart rectangle
	g_chartRect.left = 45;
	g_chartRect.top = 430;
	g_chartRect.right = g_chartRect.left + 205;
	g_chartRect.bottom = g_chartRect.top + 205;

	if (!InitIniFile())
	{
		GetDataFromFile(g_expenseListview, g_lastIndex, g_currentIndex, 
			g_totalExpense, g_expensePerType, g_percentages, g_chanceFlag);
	}

	WCHAR* date = GetStartDateFromFile();
	SetWindowText(g_startDateLabel, date);
	if (date != NULL)
		delete date;

	InvalidateRect(hWnd, NULL, TRUE);
	return TRUE;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	switch (id)
	{
	case IDB_ADD:
		AddAnItem(g_totalExpense, g_expensePerType, g_percentages, g_chanceFlag,
			g_expenseListview, g_currentIndex, g_typeCombobox, g_contentTextbox, g_costTextbox);
		InvalidateRect(hWnd, &g_chartRect, FALSE);
		SetWindowText(g_contentTextbox, L"");
		SetWindowText(g_costTextbox, L"");
		break;
	case  IDB_CLEAR:
	{
		if (MessageBox(hWnd, L"Bạn có muốn xóa lịch sử chi tiêu?", L"Xác nhận", MB_OKCANCEL) == IDOK)
		{
			ListView_DeleteAllItems(g_expenseListview);

			for (unsigned int i = 0; i < 6; i++)
			{
				g_expensePerType[i] = 0;
				g_percentages[i] = 0;
			}
			g_totalExpense = 0;

			g_lastIndex = 0;
			g_currentIndex = 0;

			g_clearFlag = true;
			g_chanceFlag = false;

			WCHAR* date = GetCurrentDate();
			SetWindowText(g_startDateLabel, date);
			if (date != NULL)
				delete date;

			InvalidateRect(hWnd, &g_chartRect, TRUE);
		}
	}
		break;
	case IDB_EXIT:
		SendMessage(hWnd, WM_CLOSE, 0, 0);
		break;
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		SendMessage(hWnd, WM_CLOSE, 0, 0);
		break;
	}
}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	// TODO: Add any drawing code that uses hdc here...
	vector<COLORREF> colors = { RGB(255,0,0), RGB(0,255,0), RGB(0,0,255),
		RGB(255,255,0), RGB(0,255,255), RGB(255,0,255) };

	// Draw annotate
	SelectObject(hdc, CreateSolidBrush(colors[0]));
	Rectangle(hdc, 290, 470, 303, 483);

	SelectObject(hdc, CreateSolidBrush(colors[1]));
	Rectangle(hdc, 290, 520, 303, 533);

	SelectObject(hdc, CreateSolidBrush(colors[2]));
	Rectangle(hdc, 290, 570, 303, 583);

	SelectObject(hdc, CreateSolidBrush(colors[3]));
	Rectangle(hdc, 460, 470, 473, 483);

	SelectObject(hdc, CreateSolidBrush(colors[4]));
	Rectangle(hdc, 460, 520, 473, 533);

	SelectObject(hdc, CreateSolidBrush(colors[5]));
	Rectangle(hdc, 460, 570, 473, 583);
	//------------------------------------------------

	// Update chart
	DrawChart(hdc, colors);

	//Update total expense
	WCHAR buffer[MAX_LOADSTRING];
	wsprintf(buffer, L"%d", g_totalExpense);
	SetWindowText(g_totalExpenseTextbox, buffer);

	ReleaseDC(hWnd, hdc);
	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hwnd)
{
	PostQuitMessage(0);
}

void OnClose(HWND hwnd)
{
	if (g_clearFlag)
		ResetIniFile(g_startDateLabel);

	if (g_chanceFlag)
		SaveDataToFile(g_expenseListview, g_lastIndex, g_currentIndex);

	DestroyWindow(hwnd);
}

void DrawChart(HDC hdc, vector<COLORREF> &colors)
{
	float lastLeft = -90.0f;
	float sweepAngel = 0;
	Color color;
	Graphics graphic(hdc);
	graphic.SetSmoothingMode(SmoothingModeAntiAlias);
	WCHAR buffer[7];
	for (unsigned int i = 0; i < 6; i++)
	{
		// Draw pie chart
		sweepAngel = (float)DEGREE_PER_PERCENT * g_percentages[i];
		color.SetFromCOLORREF(colors[i]);
		SolidBrush solidBrush(color);
		graphic.FillPie(&solidBrush, g_chartRect.left, g_chartRect.top, 
			200, 200, lastLeft, sweepAngel);
		lastLeft += sweepAngel;

		// Update annotate
		ZeroMemory(buffer, 7 * sizeof(WCHAR));
		wsprintf(buffer, L"%d %%", g_percentages[i]);
		SetWindowText(g_percentageLabels[i], buffer);
	}
}