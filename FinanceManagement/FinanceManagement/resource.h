//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FinanceManagement.rc
//
#define IDC_MYICON                      2
#define IDD_FINANCEMANAGEMENT_DIALOG    102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_FINANCEMANAGEMENT           107
#define IDI_SMALL                       108
#define IDC_FINANCEMANAGEMENT           109
#define IDB_ADD                         110
#define IDB_CLEAR                       111
#define IDB_EXIT                        112
#define ID_CBBTYPE                      113
#define IDR_MAINFRAME                   128
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           114
#endif
#endif
