#pragma once
#include <vector>
using namespace std;

//
//	FUNCTION: bool InitIniFile()
//
//	PURPOSE: Initial ini file
//
bool InitIniFile();

//
//	FUNCTION: void ResetIniFile(_in_ HWND)
//
//	PURPOSE: Reset ini file
//
//	PARAMETER:
//
//			HWND			dateLabel		Handle of label contain start tracking date
//
void ResetIniFile(HWND dateLabel);

//
//	FUNCTION: void GetDataFromFile(_Out_ HWND, _Out_ int&, _Out_ int&
//									_Out_ int&, _Out_ vector<int>&, _Out_ vector<int>&, _Out_ bool&)
//
//	PURPOSE: Load data of expense from ini file
//
//	PARAMETER:
//
//			HWND			listviewHandle		handle of list view
//			int&			lastItemIndex		index of last item after load data
//			int&			currentItemIndex:	the index of item in list view for adding
//			int&			totalExpense:		the total of expense
//			vector<int>&	expensePerType:		value of each type
//			vector<int>&	percentages:		percent of each type
//			bool&			changeFlag:			the flag of data chance
//
void GetDataFromFile(HWND listviewHandle, int &lastItemIndex, int &currentItemIndex,
	int &totalexpense, vector<int> &expensePerType, vector<int> &percentages,
	bool &changeFlag);

//
//	FUNCTION: void GetDataFromFile(_In_ HWND, _In_ int&, _In_ int&)
//
//	PURPOSE: Save data to ini file
//
//	PARAMETER:
//
//			HWND			listviewHandle		handle of list view
//			int&			lastItemIndex		index of last item after starting
//			int&			currentItemIndex:	index of latest item
//
void SaveDataToFile(HWND listviewHandle, int &lastItemIndex, int &currentItemIndex);

//
//	FUNCTION: WCHAR* GetCurrentDate()
//
//	PURPOSE: Get current date.
//
WCHAR* GetCurrentDate();

//
//	FUNCTION: WCHAR* GetStartDateFromFile()
//
//	PURPOSE: Get start date of tracking expense from ini file.
//
WCHAR* GetStartDateFromFile();