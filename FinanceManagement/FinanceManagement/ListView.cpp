﻿#include "stdafx.h"
#include "ListView.h"

void ComputePercentage(int type, int cost, int &totalexpense,
	vector<int> &expensePerType, vector<int> &percentages)
{
	totalexpense += cost;
	expensePerType[type] += cost;
	int totalPercent = 0;

	for (unsigned int i = 0; i < 6; i++)
	{
		percentages[i] = expensePerType[i] * 100 / totalexpense;
		totalPercent += percentages[i];
	}

	int rem = totalexpense / 2;
	int temp = 0;
	while (totalPercent < 100)
	{
		while (true)
		{
			if (((expensePerType[temp] * 100) % totalexpense) > rem
				&& expensePerType[temp] != 0)
			{
				percentages[temp]++;
				totalPercent++;
				temp = (temp + 1) % 6;
				break;
			}
			temp = (temp + 1) % 6;
		}
	}
}

void AddAnItem(int &totalexpense, vector<int> &expensePerType, vector<int> &percentages,
	bool &changeFlag, HWND listviewHandle, int &currentIndex, int &type, WCHAR* content, WCHAR* cost)
{
	if (content == NULL || cost == NULL)
		return;

	LVITEM lvItem;
	lvItem.mask = LVIF_TEXT | LVIF_PARAM;
	lvItem.iItem = currentIndex;
	lvItem.iSubItem = 0;

	WCHAR buffer[MAX_LOADSTRING];
	ZeroMemory(buffer, MAX_LOADSTRING * sizeof(WCHAR));
	switch (type)
	{
	case food:
		wsprintf(buffer, L"Ăn uống");
		break;
	case transport:
		wsprintf(buffer, L"Di chuyển");
		break;
	case living:
		wsprintf(buffer, L"Nhà cửa");
		break;
	case vehicle:
		wsprintf(buffer, L"Xe cộ");
		break;
	case necessary:
		wsprintf(buffer, L"Nhu yếu phẩm");
		break;
	case service:
		wsprintf(buffer, L"Dịch vụ");
		break;
	}
	lvItem.pszText = buffer;
	lvItem.lParam = (LPARAM)type;
	ListView_InsertItem(listviewHandle, &lvItem);

	ListView_SetItemText(listviewHandle, currentIndex, 1, content);
	

	int costValue;
	ListView_SetItemText(listviewHandle, currentIndex, 2, cost);
	costValue = _wtoi(cost);

	// Update percent for each type of expense
	ComputePercentage(type, costValue, totalexpense, expensePerType, percentages);

	currentIndex++;
	changeFlag = true;
}

void AddAnItem(int &totalexpense, vector<int> &expensePerType, vector<int> &percentages,
	bool &changeFlag, HWND listviewHandle, int &currentIndex, 
	HWND typeCombobox, HWND contentTextbox, HWND costTextbox)
{
	if (GetWindowTextLength(contentTextbox) == 0
		|| GetWindowTextLength(costTextbox) == 0)
		return;

	LVITEM lvItem;
	lvItem.mask = LVIF_TEXT | LVIF_PARAM;
	lvItem.iItem = currentIndex;
	lvItem.iSubItem = 0;

	int selectIndex = ComboBox_GetCurSel(typeCombobox);
	
	WCHAR buffer[MAX_LOADSTRING];
	ZeroMemory(buffer, MAX_LOADSTRING * sizeof(WCHAR));
	switch (selectIndex)
	{
	case food:
		wsprintf(buffer, L"Ăn uống");
		break;
	case transport:
		wsprintf(buffer, L"Di chuyển");
		break;
	case living:
		wsprintf(buffer, L"Nhà cửa");
		break;
	case vehicle:
		wsprintf(buffer, L"Xe cộ");
		break;
	case necessary:
		wsprintf(buffer, L"Nhu yếu phẩm");
		break;
	case service:
		wsprintf(buffer, L"Dịch vụ");
		break;
	}
	lvItem.pszText = buffer;
	lvItem.lParam = (LPARAM)selectIndex;
	ListView_InsertItem(listviewHandle, &lvItem);

	ZeroMemory(buffer, MAX_LOADSTRING * sizeof(WCHAR));
	GetWindowText(contentTextbox, buffer, MAX_LOADSTRING);
	ListView_SetItemText(listviewHandle, currentIndex, 1, buffer);

	int costValue;
	ZeroMemory(buffer, MAX_LOADSTRING * sizeof(WCHAR));
	GetWindowText(costTextbox, buffer, MAX_LOADSTRING);
	ListView_SetItemText(listviewHandle, currentIndex, 2, buffer);
	costValue = _wtoi(buffer);
	
	// Update percent for each type of expense
	ComputePercentage(selectIndex, costValue, totalexpense, expensePerType, percentages);

	currentIndex++;
	changeFlag = true;
}