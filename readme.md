# README #

## Information ##
* ID: 1512077
* Name: Ho Xuan Dung

## Configuration System ##
* OS: Windows 10 x64
* IDE: Visual Studio 2017

## Function ##
* Add an expense.
* Show the list of expense.
* Draw a pie chart to show statistics.
* Save and load expense list from ini file with UTF-16 LE BOM encoding.
* Change focus of control using TAB key.
* Clear expense list.
* Log the start date of tracking expense.

## Application Flow ##
### Main Flow ###
* Choose a type of expense from combobox, then type the content and cost to two textbox, press button Add.
* Press Clear button to clear expense list.
* When application is started, data from ini file will be loaded automatically. When it's cloased, data will be save to ini file.

### Addiction Flow ###
* When you miss typing either content textbox or cost textbox and press add, nothing will be add to expense list.

## BitBucket Link ##
* https://peterho249@bitbucket.org/peterho249/financemanagement.git

## Youtube Link ##
* https://youtu.be/bMnm1vJKEVU